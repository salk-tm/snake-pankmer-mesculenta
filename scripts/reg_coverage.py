from pankmer import PKResults, reg_coverage
reg_coverage(*(PKResults(x) for x in snakemake.input.index),
    ref=snakemake.input.target_genome,
    coords=snakemake.params.gene,
    output_file=snakemake.output.bdg,
    bgzip=True,
    flank=2000,
    genes=snakemake.input.target_features,
    processes=snakemake.threads
)