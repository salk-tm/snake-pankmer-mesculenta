from pankmer import PKResults, reg_coverage
for index, output in zip(snakemake.input.indexes, snakemake.output.bdg):
    reg_coverage(PKResults(index), ref=snakemake.input.target_genome,
        coords=f'{snakemake.params.contig}:1-{snakemake.params.contig_size}',
        output_file=output, bgzip=True)
