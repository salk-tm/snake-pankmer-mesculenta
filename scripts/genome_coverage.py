from pankmer import PKResults, genome_coverage

genome_coverage(*(PKResults(i) for i in snakemake.input.indexes),
    ref=snakemake.input.target_genome,
    chromosomes=[snakemake.params.contig],
    output=snakemake.output.svg,
    output_table=snakemake.output.table,
    # groups=snakemake.params.groups,
    title=snakemake.params.title,
    # x_label=snakemake.params.x_label,
    # legend=True,
    # legend_title=snakemake.params.legend_title,
    # legend_loc='outside',
    # color_palette=snakemake.params.color_palette,
    processes=snakemake.threads,
    bin_size=snakemake.params.bin_size
)